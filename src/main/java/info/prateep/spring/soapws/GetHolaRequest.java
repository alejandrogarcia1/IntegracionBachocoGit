//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.05.13 a las 03:56:46 PM UTC 
//


package info.prateep.spring.soapws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HEADER">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Id_transaccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FREIGHT_ORDER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Estatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PLANNED_KM" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="PLANNED_TIME" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="VEHICLE_RESOURCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MTR_COMBINATION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CARRIER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DRIVER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="STAGES">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="STAGE_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ORIGEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DESTINO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MTR_COMBINATION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EVENT">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ID_EVENTO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LOCATION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DATE" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                             &lt;element name="HOUR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "stages"
})
@XmlRootElement(name = "getHolaRequest")
public class GetHolaRequest {

    @XmlElement(name = "HEADER", required = true)
    protected GetHolaRequest.HEADER header;
    @XmlElement(name = "STAGES", required = true)
    protected GetHolaRequest.STAGES stages;

    /**
     * Obtiene el valor de la propiedad header.
     * 
     * @return
     *     possible object is
     *     {@link GetHolaRequest.HEADER }
     *     
     */
    public GetHolaRequest.HEADER getHEADER() {
        return header;
    }

    /**
     * Define el valor de la propiedad header.
     * 
     * @param value
     *     allowed object is
     *     {@link GetHolaRequest.HEADER }
     *     
     */
    public void setHEADER(GetHolaRequest.HEADER value) {
        this.header = value;
    }

    /**
     * Obtiene el valor de la propiedad stages.
     * 
     * @return
     *     possible object is
     *     {@link GetHolaRequest.STAGES }
     *     
     */
    public GetHolaRequest.STAGES getSTAGES() {
        return stages;
    }

    /**
     * Define el valor de la propiedad stages.
     * 
     * @param value
     *     allowed object is
     *     {@link GetHolaRequest.STAGES }
     *     
     */
    public void setSTAGES(GetHolaRequest.STAGES value) {
        this.stages = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Id_transaccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FREIGHT_ORDER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Estatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PLANNED_KM" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="PLANNED_TIME" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="VEHICLE_RESOURCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MTR_COMBINATION" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CARRIER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DRIVER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idTransaccion",
        "freightorder",
        "estatus",
        "plannedkm",
        "plannedtime",
        "vehicleresource",
        "mtrcombination",
        "carrier",
        "driver"
    })
    public static class HEADER {

        @XmlElement(name = "Id_transaccion", required = true)
        protected String idTransaccion;
        @XmlElement(name = "FREIGHT_ORDER", required = true)
        protected String freightorder;
        @XmlElement(name = "Estatus", required = true)
        protected String estatus;
        @XmlElement(name = "PLANNED_KM", required = true)
        protected BigDecimal plannedkm;
        @XmlElement(name = "PLANNED_TIME", required = true)
        protected BigDecimal plannedtime;
        @XmlElement(name = "VEHICLE_RESOURCE", required = true)
        protected String vehicleresource;
        @XmlElement(name = "MTR_COMBINATION", required = true)
        protected String mtrcombination;
        @XmlElement(name = "CARRIER", required = true)
        protected String carrier;
        @XmlElement(name = "DRIVER", required = true)
        protected String driver;

        /**
         * Obtiene el valor de la propiedad idTransaccion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdTransaccion() {
            return idTransaccion;
        }

        /**
         * Define el valor de la propiedad idTransaccion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdTransaccion(String value) {
            this.idTransaccion = value;
        }

        /**
         * Obtiene el valor de la propiedad freightorder.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFREIGHTORDER() {
            return freightorder;
        }

        /**
         * Define el valor de la propiedad freightorder.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFREIGHTORDER(String value) {
            this.freightorder = value;
        }

        /**
         * Obtiene el valor de la propiedad estatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEstatus() {
            return estatus;
        }

        /**
         * Define el valor de la propiedad estatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEstatus(String value) {
            this.estatus = value;
        }

        /**
         * Obtiene el valor de la propiedad plannedkm.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPLANNEDKM() {
            return plannedkm;
        }

        /**
         * Define el valor de la propiedad plannedkm.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPLANNEDKM(BigDecimal value) {
            this.plannedkm = value;
        }

        /**
         * Obtiene el valor de la propiedad plannedtime.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPLANNEDTIME() {
            return plannedtime;
        }

        /**
         * Define el valor de la propiedad plannedtime.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPLANNEDTIME(BigDecimal value) {
            this.plannedtime = value;
        }

        /**
         * Obtiene el valor de la propiedad vehicleresource.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVEHICLERESOURCE() {
            return vehicleresource;
        }

        /**
         * Define el valor de la propiedad vehicleresource.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVEHICLERESOURCE(String value) {
            this.vehicleresource = value;
        }

        /**
         * Obtiene el valor de la propiedad mtrcombination.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMTRCOMBINATION() {
            return mtrcombination;
        }

        /**
         * Define el valor de la propiedad mtrcombination.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMTRCOMBINATION(String value) {
            this.mtrcombination = value;
        }

        /**
         * Obtiene el valor de la propiedad carrier.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCARRIER() {
            return carrier;
        }

        /**
         * Define el valor de la propiedad carrier.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCARRIER(String value) {
            this.carrier = value;
        }

        /**
         * Obtiene el valor de la propiedad driver.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDRIVER() {
            return driver;
        }

        /**
         * Define el valor de la propiedad driver.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDRIVER(String value) {
            this.driver = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="STAGE_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ORIGEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DESTINO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MTR_COMBINATION" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EVENT">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID_EVENTO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LOCATION" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *                   &lt;element name="HOUR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stageid",
        "origen",
        "destino",
        "mtrcombination",
        "event"
    })
    public static class STAGES {

        @XmlElement(name = "STAGE_ID", required = true)
        protected String stageid;
        @XmlElement(name = "ORIGEN", required = true)
        protected String origen;
        @XmlElement(name = "DESTINO", required = true)
        protected String destino;
        @XmlElement(name = "MTR_COMBINATION", required = true)
        protected String mtrcombination;
        @XmlElement(name = "EVENT", required = true)
        protected GetHolaRequest.STAGES.EVENT event;

        /**
         * Obtiene el valor de la propiedad stageid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSTAGEID() {
            return stageid;
        }

        /**
         * Define el valor de la propiedad stageid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSTAGEID(String value) {
            this.stageid = value;
        }

        /**
         * Obtiene el valor de la propiedad origen.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getORIGEN() {
            return origen;
        }

        /**
         * Define el valor de la propiedad origen.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setORIGEN(String value) {
            this.origen = value;
        }

        /**
         * Obtiene el valor de la propiedad destino.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTINO() {
            return destino;
        }

        /**
         * Define el valor de la propiedad destino.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTINO(String value) {
            this.destino = value;
        }

        /**
         * Obtiene el valor de la propiedad mtrcombination.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMTRCOMBINATION() {
            return mtrcombination;
        }

        /**
         * Define el valor de la propiedad mtrcombination.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMTRCOMBINATION(String value) {
            this.mtrcombination = value;
        }

        /**
         * Obtiene el valor de la propiedad event.
         * 
         * @return
         *     possible object is
         *     {@link GetHolaRequest.STAGES.EVENT }
         *     
         */
        public GetHolaRequest.STAGES.EVENT getEVENT() {
            return event;
        }

        /**
         * Define el valor de la propiedad event.
         * 
         * @param value
         *     allowed object is
         *     {@link GetHolaRequest.STAGES.EVENT }
         *     
         */
        public void setEVENT(GetHolaRequest.STAGES.EVENT value) {
            this.event = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID_EVENTO" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LOCATION" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
         *         &lt;element name="HOUR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idevento",
            "location",
            "sequence",
            "date",
            "hour"
        })
        public static class EVENT {

            @XmlElement(name = "ID_EVENTO", required = true)
            protected String idevento;
            @XmlElement(name = "LOCATION", required = true)
            protected String location;
            @XmlElement(name = "SEQUENCE", required = true)
            protected String sequence;
            @XmlElement(name = "DATE", required = true)
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar date;
            @XmlElement(name = "HOUR", required = true)
            protected String hour;

            /**
             * Obtiene el valor de la propiedad idevento.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIDEVENTO() {
                return idevento;
            }

            /**
             * Define el valor de la propiedad idevento.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIDEVENTO(String value) {
                this.idevento = value;
            }

            /**
             * Obtiene el valor de la propiedad location.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLOCATION() {
                return location;
            }

            /**
             * Define el valor de la propiedad location.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLOCATION(String value) {
                this.location = value;
            }

            /**
             * Obtiene el valor de la propiedad sequence.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSEQUENCE() {
                return sequence;
            }

            /**
             * Define el valor de la propiedad sequence.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSEQUENCE(String value) {
                this.sequence = value;
            }

            /**
             * Obtiene el valor de la propiedad date.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDATE() {
                return date;
            }

            /**
             * Define el valor de la propiedad date.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDATE(XMLGregorianCalendar value) {
                this.date = value;
            }

            /**
             * Obtiene el valor de la propiedad hour.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHOUR() {
                return hour;
            }

            /**
             * Define el valor de la propiedad hour.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHOUR(String value) {
                this.hour = value;
            }

        }

    }

}
