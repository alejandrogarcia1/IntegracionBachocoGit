//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.05.13 a las 03:56:46 PM UTC 
//


package info.prateep.spring.soapws;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the info.prateep.spring.soapws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: info.prateep.spring.soapws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetHolaRequest }
     * 
     */
    public GetHolaRequest createGetHolaRequest() {
        return new GetHolaRequest();
    }

    /**
     * Create an instance of {@link GetHolaRequest.STAGES }
     * 
     */
    public GetHolaRequest.STAGES createGetHolaRequestSTAGES() {
        return new GetHolaRequest.STAGES();
    }

    /**
     * Create an instance of {@link GetStateRequest }
     * 
     */
    public GetStateRequest createGetStateRequest() {
        return new GetStateRequest();
    }

    /**
     * Create an instance of {@link GetHolaResponse }
     * 
     */
    public GetHolaResponse createGetHolaResponse() {
        return new GetHolaResponse();
    }

    /**
     * Create an instance of {@link GetHolaRequest.HEADER }
     * 
     */
    public GetHolaRequest.HEADER createGetHolaRequestHEADER() {
        return new GetHolaRequest.HEADER();
    }

    /**
     * Create an instance of {@link GetStateResponse }
     * 
     */
    public GetStateResponse createGetStateResponse() {
        return new GetStateResponse();
    }

    /**
     * Create an instance of {@link State }
     * 
     */
    public State createState() {
        return new State();
    }

    /**
     * Create an instance of {@link GetHolaRequest.STAGES.EVENT }
     * 
     */
    public GetHolaRequest.STAGES.EVENT createGetHolaRequestSTAGESEVENT() {
        return new GetHolaRequest.STAGES.EVENT();
    }

}
